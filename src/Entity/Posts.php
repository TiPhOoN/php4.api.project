<?php

namespace Project4\Entity;

use DateTimeImmutable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Posts
{
    private DateTimeImmutable $postedAt;

    public function __construct(
        private UuidInterface $id,
        private string $title,
        private string $slug,
        private string $content,
        private string $thumbnail,
        private string $author,
    ) {
        $this->postedAt = new DateTimeImmutable('now');
    }

    /**
     * @throws \Exception
     */
    public static function populate(array $data): self
    {
        return new self(
            Uuid::fromString($data['id']),
            $data['title'],
            $data['slug'],
            $data['content'],
            $data['thumbnail'],
            $data['author'],
        );
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function slug(): string
    {
        return $this->slug;
    }

    public function content(): string
    {
        return $this->content;
    }

    public function thumbnail(): string
    {
        return $this->thumbnail;
    }

    public function author(): string
    {
        return $this->author;
    }

    public function postedAt(): DateTimeImmutable
    {
        return $this->postedAt;
    }
}
